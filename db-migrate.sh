#!/bin/bash

# Colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Function to generate a random sleep duration between 10 and 200 milliseconds
generate_random_sleep() {
  sleep_duration=$(( (RANDOM % 191) + 10 ))
  sleep_duration=$(awk "BEGIN{printf \"%.3f\", $sleep_duration/1000}")
  echo $sleep_duration
}

# Simulating a DDL change
echo -e "${GREEN}Updating table structure...${NC}"
# Command for DDL change
sleep 1

# Generate 52 inserts with random sleep intervals
for ((i=1; i<=4; i++)); do
  sleep_duration=$(generate_random_sleep)
  echo -e "${GREEN}Inserting record $i...${NC}"
  # Command for successful database insert
  if [[ ! -z "$sleep_duration" ]]; then
    sleep $sleep_duration
  fi
done



# Simulating an error and program exit
echo -e "${RED}Encountered an error while performing database operation!${NC}"
# Command that causes an error, e.g., an invalid SQL query

# Exiting the program with an error status
exit 1
